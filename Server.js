
const { compileFunction } = require('vm');

// Create Server
var app=require('express')();
var server=require('http').Server(app);
var io =require('socket.io')(server,{
    cors:{
        origin:"*"
    }
});


//Connecting Sockets
io.on("connection",(socket)=>{
    console.log(socket.id)
    socket.on("chat",(payload)=>{
        if (payload.room){
            socket.to(payload.room).emit("chat",payload)
        }else{
            socket.broadcast.emit("chat",payload)   
        }
        
    })
})
//Enabling one to one chat
//Enabling Group Chat("Broadcasting msgs to all users in Group")
//Adding Group
//Adding Person in Group
//Connecting with Database
//Modeling in Database


// Starting server
server.listen(3000,()=>{
    console.log("Server Started")
})




//Have to show all the User who got connected.
//By Selecting user we can chat
//Issue with sending msg to USer who are not connected.[Can be overcome by using Database]
//Can send msg to same room consider it as single socket.
